#include<stdio.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}
int fact(int n)
{
    int a,p=1;
    for(a=1;a<=n;a++)
    {
        p=p*a;
    }
    return p;
}
void output(int a,int b)
{
    printf("the factorial of %d =%d\n",a,b);
}
int main()
{
    int n,fac;
    n=input();
    fac=fact(n);
    output(n,fac);
    return 0;
}