//
//  main.c
//  palindrome
//
//  Created by Sanket on 10/16/19.
//  Copyright © 2019 Sanket. All rights reserved.
//

#include <stdio.h>

int main()
{
    // insert code here...
    int a,b,c,d;
    printf("Enter a number\n");
    scanf("%d",&a);
    c=a;
    b=0;
    while(a>0)
    {
        d=a%10;
        b=b*10+d;
        a=a/10;
    }
    if(b==c)
        printf("%d is a palindrome\n",c);
    else
        printf("%d is not a palindrome\n",c);
    return 0;
}